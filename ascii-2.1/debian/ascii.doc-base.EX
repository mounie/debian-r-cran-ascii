Document: ascii
Title: Debian ascii Manual
Author: <insert document author here>
Abstract: This manual describes what ascii is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/ascii/ascii.sgml.gz

Format: postscript
Files: /usr/share/doc/ascii/ascii.ps.gz

Format: text
Files: /usr/share/doc/ascii/ascii.text.gz

Format: HTML
Index: /usr/share/doc/ascii/html/index.html
Files: /usr/share/doc/ascii/html/*.html
