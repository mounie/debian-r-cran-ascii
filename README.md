Packaging for Debian of R CRAN ascii package

Source
======
https://cran.r-project.org/web/packages/ascii/index.html

Motivation
==========

  * My first Debian packaging
  * This package helps produce org-mode output from R object, notably
    org table
	


